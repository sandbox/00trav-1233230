This module allows a site admin to define a node type and a field name which will hold the "bundle" data. Users (with 'add node' permission for that node type) can then create a node with multiple items in the ASIN fields, and when the bundle node is displayed a link is displayed in the link section 'add this bundle to your cart.' Clicking on the link will automatically add all ASIN's located in such field for such nid to the current users cart and then redirect to the cart page.

Installation:
1. Install as normal Drupal module (e.g. unpack module download in modules directory and install via admin/modules)
2. Go to Amazon Store Bundle settings page (admin/config/amazon_settings/bundle) and specify the node type (must be the machine name) and the field (which contains the ASIN).
3. Create a new node (of the type specified above).  Add several products to the ASIN field specified above.  Then view. 